## Add noise

This data operation adds noise to an image, the types of noise available are : 
gaussian , localvar , poisson , salt , pepper and speckle. This operation uses the package Skimage specifically the class 
[utils.random\_noise](https://scikit-image.org/docs/dev/api/skimage.util.html#skimage.util.random\_noise).

#### Standard Parameter(s): 

1. `input-path`: Path to the location of the dataset. With the current version, 
it is recommended to use only one directory where all the data exists. 

2. `output-path`: Path to save the images after processing.

3. `mode`: This string represents the type of noise to be added to all the images in the dataset.It can be `gaussian` , `localvar` , `poisson` , `salt` , `pepper` and `speckle`.



