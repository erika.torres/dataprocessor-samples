cycler==0.10.0
decorator==4.4.2
imageio==2.8.0
joblib==0.14.1
keras==2.3.0
kiwisolver==1.2.0
numpy==1.19.0
opencv-python==4.2.0.34
Pillow==7.1.2
pyparsing==2.4.7
python-dateutil==2.8.1
PyWavelets==1.1.1
scikit-image==0.16.2
scipy==1.4.1
six==1.14.0
tqdm==4.50.1





