## Augment

Neural network architectures have a large number of trainable parameters and therefore they require a very large number of images to train on to effectively 
capture the distrubution of the data and 'learn'. Data augmentation is a strategy that enables us to significantly increase the diversity of data available 
for training models, without actually collecting new data. 

For example, in the case of images, the data is tweaked by changing angle of rotation, flipping the images, zooming in, etc.

#### Standard Parameter(s):  
1. `input-path`: Path to the location of the dataset. With the current version, 
it is recommended to use only one directory where all the data exists. 
2. `output-path`: Path to save the images after processing.
3. `iterations(int)`: Multiplying factor which dictates how many augmented images should be output per image in the dataset (inclusive). Must be a positive float value

#### Advanced Parameter(s):  

1. `rotation_range (float)`: Degree range for random rotations
2. `width_shift_range(float)` : 1-D array-like or INT - FLOAT: fraction of total width, if < 1, or pixels if >= 1. - 1-D array-like: random elements from the array. - INT: Integer number of pixels from Interval (-width_shift_range, +width_shift_range)
3. `height_shift_range(float)`: 1-D array-like or INT - FLOAT: fraction of total height, if < 1, or pixels if >= 1. - 1-D array-like: random elements from the array. - INT: Integer number of pixels from INTerval (-height_shift_range, +height_shift_range) - With height_shift_range=2 possible values are INTegers [-1, 0, +1], same as with height_shift_range=[-1, 0, +1], while with height_shift_range=1.0 possible values are floats in the INTerval [-1.0, +1.0).
4. `shear_range(float)` : Shear intensity (Shear angle in counter-clockwise direction in degrees)
5. `zoom_range(float)`: Zoom range from 0 none to 1.
6. `horizontal_flip(bool)`: Use of horizontal flip for augmentation
7. `vertical_flip(bool)`: Use of vertical flip for augmentation

